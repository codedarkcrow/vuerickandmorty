const path = require('path');
const { VueLoaderPlugin } = require("vue-loader");
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: path.join(__dirname, 'src', 'index.js'),
    output: {
        publicPath: '/',
        path: path.join(__dirname, 'dist'),
        filename: 'bundle-[fullhash].js',
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.common.js',
        },
        extensions: ['.vue', '.js', '.json'],
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.(vue)$/,
                loader: 'vue-loader',
            },
            {
                test: /\.(s?css)$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader',
                ],
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'public', 'index.html'),
        })
    ],
};