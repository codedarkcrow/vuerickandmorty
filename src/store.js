import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const key = 'characterList';

export const type = {
    ADD_OR_REMOVE_CHARACTER: 'addOrRemoveCharacter',
    REMOVE_CHARACTER: 'removeCharacter',
};

export default new Vuex.Store({
    state: {
        favoriteCharacters: JSON.parse(window.localStorage.getItem(key)) || [],
    },
    mutations: {
        [type.REMOVE_CHARACTER](state, payload) {
            state.favoriteCharacters = state.favoriteCharacters.filter(character => character.id !== payload.id);
            window.localStorage.setItem(key, JSON.stringify(state.favoriteCharacters));
        },
        [type.ADD_OR_REMOVE_CHARACTER](state, payload) {
            const isInList = Boolean(state.favoriteCharacters.filter(character => character.id === payload.character.id).length);
            state.favoriteCharacters = isInList ?
                state.favoriteCharacters.filter(character => character.id !== payload.character.id)
                : [...state.favoriteCharacters, payload.character];
            window.localStorage.setItem(key, JSON.stringify(state.favoriteCharacters));
        }
    },
});