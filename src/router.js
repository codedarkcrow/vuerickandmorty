import Vue from 'vue';
import VueRouter from 'vue-router';
import { HomePage, AboutPage, CharacterDetailPage, FavoritePage } from './containers';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: HomePage,
    },
    {
        path: '/about',
        name: 'about',
        component: AboutPage,
    },
    {
        path: '/characterDetail/:id',
        name: 'detail',
        component: CharacterDetailPage,
    },
    {
        path: '/favorite',
        name: 'favorite',
        component: FavoritePage,
    },
];

const router = new VueRouter({
    routes,
});

export default router;