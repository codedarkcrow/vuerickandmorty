import HomePage from './HomePage';
import AboutPage from './AboutPage';
import CharacterDetailPage from './CharacterDetailPage';
import FavoritePage from './FavoritePage';

export {
    HomePage,
    AboutPage,
    CharacterDetailPage,
    FavoritePage,
};