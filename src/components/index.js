import CharacterCard from './CharacterCard.vue';
import CharacterCarousel from './CharacterCarousel.vue';
import Header from './Header.vue';
import MainCard from './MainCard.vue';
import PaginationButton from './PaginationButton';
import SearchBox from './SearchBox';

export {
    CharacterCard,
    CharacterCarousel,
    Header,
    MainCard,
    PaginationButton,
    SearchBox,
}